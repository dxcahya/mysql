


1.membuat database


create database myshop;

2.membuat table

create table categories(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );

create table items(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(5),
    -> stok int(66),
    -> categories_id int(8),
    -> primary key(id),
    -> foreign key(categories_id) references categories(id)
    -> );

create table users(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

3. memasukan data

memasukan data users 

 insert into users(name, email, password) values("John Doe", "john@doe.com", "john123");
Query OK, 1 row affected (0.403 sec)

MariaDB [myshop]> insert into users(name, email, password) values("John Doe", "jane@doe.com", "jenita123");

memasukan data di categories

insert into categories(name) values("gadget"), ("cloth"), ("men"), ("women"), ("branded");

memasukan data di items

insert into items(name, description, price, stok, categories_id) values("sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1);
insert into items(name, description, price, stok, categories_id) values("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2);
insert into items(name, description, price, stok, categories_id) values("IMHO watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


4.Mengambil Data dari Database

a. select id, name, email from users;

b.select * from items where price > 1000000;

b2.select * from items where name like  '%sang%';

c.select items.name, items.description, items.price, items.stok, items.categories_id, categories.name from items inner JOIN categories on items.categories_id = categories.id;

5. Menampilkan data items join dengan kategori

UPDATE items SET price ='2500000' WHERE name='sumsang b50';

